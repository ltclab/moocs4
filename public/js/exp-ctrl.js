var app = app || {};
app.expCtrl = (function(){

  var prefix = 'ltclabMoocs';

  function setStorage(key, value){
    localStorage[getStorageKey(key)] = value;
  }

  function getStorage(key){
    return localStorage[getStorageKey(key)];
  }

  function getStorageKey(key){
    return prefix + key;
  }

  function setUserCode(userCode){
    setStorage('UserCode', userCode);
  }

  function getUserCode(){
    return getStorage('UserCode');
  }

  function setGroup(group){
    setStorage('Group', group);
  }

  function getGroup(){
    return getStorage('Group');
  }

  function setDebug(debug){
    setStorage('Debug', debug);
  }

  function getDebug(){
    return getStorage('Debug') === 'true';
  }

    function setStartingTime(time){
    setStorage('StartingTime', time);
  }

    function getStartingTime(){
    return getStorage('StartingTime');
  }

  return {
    setUserCode: setUserCode,
    getUserCode: getUserCode,
    setGroup: setGroup,
    getGroup: getGroup,
    setDebug: setDebug,
    getDebug: getDebug,
    setStartingTime:setStartingTime,
    getStartingTime:getStartingTime,
  };

})();