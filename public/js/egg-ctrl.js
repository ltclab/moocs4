var app = app || {};
app.eggCtrl = (function(){
  var keyCodedMap = {
    '38': 'up', '40': 'down', '37': 'left', '39': 'right'
  };
  var matchKeyboardsStr = 'up up down down left left right right';

  var inputKeyboards = [];
  var handlers = [];

  addEventListener(document);

  function addEventListener(_document){
    _document.addEventListener( "keydown", function(event){
      var keyboard = keyCodedMap[event.keyCode];
      if ( keyboard ) {
        inputKeyboards.push(keyboard);
        if ( inputKeyboards.join(' ').indexOf(matchKeyboardsStr) !== -1 ) {
          inputKeyboards = [];
          handlers.forEach(function(handler){
            handler();
          });
        }
      } else {
        inputKeyboards = [];
      }
    }, false );
  }

  function addHandler(handler){
    handlers.push(handler);
  }

  function keyboards2Str(keyboards){
    return keyboards.map(function(keyboard){
      return keyCodedMap[keyboard];
    }).join(' ');
  }

  return {
    addHandler: addHandler,
    addEventListener: addEventListener
  };
})();

app.eggCtrl.addHandler(function(){
  app.expCtrl.setDebug(!app.expCtrl.getDebug());
  if ( app.expCtrl.getDebug() ) {
    alert('啟動除錯模式');
  } else {
    alert('關閉除錯模式');
  }
  location.reload();
});

$(function(){
 // var debugPromptHtml = '<div class="debug-prompt" style="position: absolute; left: 0; right: 0; width: 100%; display: flex; justify-content: center;"><span class="right-align red white-text" style="font-size: 32px;background: white;padding: 0 50px 0 50px;">開發者模式</span></div>';
   var debugPromptHtml = '<nav class="grey darken-3"> <div class="nav-wrapper container"><div class="debug-prompt" style="position: absolute; left: 0; right: 0; width: 100%; display: flex; justify-content: center;"><span class="right-align red white-text" style="font-size: 32px;background: white;padding: 0 50px 0 50px;">開發者模式</span></div></div></nav>';
   var debugPromptHtml2 = '<div class="row"><div class="col-lg-12 center-align"><a class="waves-effect waves-light btn" href="#!" onclick="app.ctrl.done()"><i class="material-icons right">navigate_next</i>跳過工作記憶</a></div></div>';


  if ( app.expCtrl.getDebug() ) {
    $('.nav-wrapper').prepend(debugPromptHtml);
    $('.buttom-wrapper').prepend(debugPromptHtml2);
  }
});