$(document).ready(function() {
  $('select').material_select();
  $('ul.tabs').tabs({
    swipeable: true
  });
});

function autoHeightIframe(iframe) {
	setTimeout(function(){
  	iframe.style.height = iframe.contentWindow.document.body.offsetHeight + 10 + 'px';
  }, 10);
  setTimeout(function(){
  	iframe.style.height = iframe.contentWindow.document.body.offsetHeight + 10 + 'px';
  }, 30);
  setTimeout(function(){
  	iframe.style.height = iframe.contentWindow.document.body.offsetHeight + 10 + 'px';
  }, 70);
  setTimeout(function(){
  	iframe.style.height = iframe.contentWindow.document.body.offsetHeight + 10 + 'px';
  }, 150);
  setTimeout(function(){
  	iframe.style.height = iframe.contentWindow.document.body.offsetHeight + 10 + 'px';
  }, 250);
}

var app = app || {};
app.debug = app.expCtrl.getDebug();