var app = app || {};
app.event = (function(){

  function create(event){
    event.category = event.category || location.pathname;
    event.userCode = event.userCode || app.expCtrl.getUserCode();
    event.time = event.time || (new Date()).toISOString()
    return event;
  }

  return {
    create: create
  };

})();