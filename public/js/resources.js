var app = app || {};
app.resources = app.resources || {};
app.resources.events = {
  post: function(event){
    var $promise = $.ajax({
      url: '/api/events',
      type: 'POST',
      dataType: "json",
      data: event
    });

    var promise = Promise.resolve($promise);
    return promise;
  }
};



app.resources.usercode = {
  get: function(){
    var $promise = $.ajax({
      url: '/api/me',
      type: 'GET',
      dataType: "json",
    });

    var promise = Promise.resolve($promise);
    return promise;
  }
};


