var express = require('express');
var router = express.Router();
var rp = require('request-promise');
var config = require('config');
var cmsData = require('../lib/cms-data');
var _ = require('lodash');

router.post('/survey-results', function(req, res, next) {
  cmsData.postSurveyResults(req).then(function(result){
    res.json(result);
  });
});

router.post('/events', function(req, res, next) {
  cmsData.postEvents(req).then(function(result){
    res.json(result);
  });
});


router.get('/me', function(req, res, next){

  if (req.user) {
    res.json(req.user);
  } else {
    res.sendStatus(204);
  }
});

module.exports = router;