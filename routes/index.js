var express = require('express');
var router = express.Router();
var cmsData = require('../lib/cms-data');

/* GET home page. */

router.get('/exp1-simple-profile', function(req, res, next) {
	cmsData.getSurvey('exp1-simple-profile').then(function(survey){
	  res.render('exp1-simple-profile', { survey: `/proxy-survey/surveys/${survey._id}` });
	});
});


router.get('/', function(req, res, next) {
  res.redirect('/welcome_content');
});

router.get('/setting', function(req, res, next) {
  res.render('setting');
});



router.get('/welcome_content', function(req, res, next) {
	cmsData.getInstruction('welcome_content').then(function(instruction){
	  res.render('welcome_content', { instruction: instruction, user: req.user });
	});
});


router.get('/starting_content', function(req, res, next) {
	cmsData.getInstruction('starting_content').then(function(instruction){
	  res.render('starting_content', { instruction: instruction });
	});
});


router.get('/intro_content', function(req, res, next) {
	cmsData.getInstruction('intro_content').then(function(instruction){
	  res.render('intro_content', { instruction: instruction });
	});
});


router.get('/pretest_content', function(req, res, next) {
	cmsData.getInstruction('pretest_content').then(function(instruction){
	  res.render('pretest_content', { instruction: instruction });
	});
});


router.get('/video_content', function(req, res, next) {
	cmsData.getInstruction('video_content').then(function(instruction){
	  res.render('video_content', { instruction: instruction });
	});
});


router.get('/video_contentA', function(req, res, next) {
	cmsData.getInstruction('video_contentA').then(function(instruction){
	  res.render('video_contentA', { instruction: instruction });
	});
});


router.get('/video_contentB', function(req, res, next) {
	cmsData.getInstruction('video_contentB').then(function(instruction){
	  res.render('video_contentB', { instruction: instruction });
	});
});


router.get('/video_contentC', function(req, res, next) {
	cmsData.getInstruction('video_contentC').then(function(instruction){
	  res.render('video_contentC', { instruction: instruction });
	});
});


router.get('/video_contentD', function(req, res, next) {
	cmsData.getInstruction('video_contentD').then(function(instruction){
	  res.render('video_contentD', { instruction: instruction });
	});
});

router.get('/posttest_content', function(req, res, next) {
	cmsData.getInstruction('posttest_content').then(function(instruction){
	  res.render('posttest_content', { instruction: instruction });
	});
});


router.get('/ending_content', function(req, res, next) {
	cmsData.getInstruction('ending_content').then(function(instruction){
	  res.render('ending_content', { instruction: instruction });
	});
});





router.get('/personal_info', function(req, res, next) {
	cmsData.getSurvey('personal_info').then(function(survey){
	  res.render('personal_info', { survey: `/proxy-survey/surveys/${survey._id}` });
	});
});


router.get('/pre_test', function(req, res, next) {
	cmsData.getSurvey('pre_test').then(function(survey){
	  res.render('pre_test', { survey: `/proxy-survey/surveys/${survey._id}` });
	});
});


router.get('/pre_test_load', function(req, res, next) {
	cmsData.getSurvey('pre_test_load').then(function(survey){
	  res.render('pre_test_load', { survey: `/proxy-survey/surveys/${survey._id}` });
	});
});


router.get('/video_load', function(req, res, next) {
	cmsData.getSurvey('video_load').then(function(survey){
	  res.render('video_load', { survey: `/proxy-survey/surveys/${survey._id}` });
	});
});


router.get('/post_test', function(req, res, next) {
	cmsData.getSurvey('post_test').then(function(survey){
	  res.render('post_test', { survey: `/proxy-survey/surveys/${survey._id}` });
	});
});


router.get('/post_test_load', function(req, res, next) {
	cmsData.getSurvey('post_test_load').then(function(survey){
	  res.render('post_test_load', { survey: `/proxy-survey/surveys/${survey._id}` });
	});
});


router.get('/self_report', function(req, res, next) {
	cmsData.getSurvey('self_report').then(function(survey){
	  res.render('self_report', { survey: `/proxy-survey/surveys/${survey._id}` });
	});
});


router.get('/problem_report', function(req, res, next) {
	cmsData.getSurvey('problem_report').then(function(survey){
	  res.render('problem_report', { survey: `/proxy-survey/surveys/${survey._id}` });
	});
});


router.get('/problem_reportA', function(req, res, next) {
	cmsData.getSurvey('problem_reportA').then(function(survey){
	  res.render('problem_reportA', { survey: `/proxy-survey/surveys/${survey._id}` });
	});
});


router.get('/problem_reportB', function(req, res, next) {
	cmsData.getSurvey('problem_reportB').then(function(survey){
	  res.render('problem_reportB', { survey: `/proxy-survey/surveys/${survey._id}` });
	});
});


router.get('/problem_reportC', function(req, res, next) {
	cmsData.getSurvey('problem_reportC').then(function(survey){
	  res.render('problem_reportC', { survey: `/proxy-survey/surveys/${survey._id}` });
	});
});


router.get('/problem_reportD', function(req, res, next) {
	cmsData.getSurvey('problem_reportD').then(function(survey){
	  res.render('problem_reportD', { survey: `/proxy-survey/surveys/${survey._id}` });
	});
});
router.get('/video_a', function(req, res, next) {
	var group = req.query.group;
	console.log(group);
	var video = `video_a`;
	cmsData.getVideo(video).then(function(result){
		console.log(result);
    res.render('video_a', { video: result.video ? result.video.url : '', webvtt: result.webvtt ? result.webvtt.url : '', pauseAt: result.pasueAt ? encodeURIComponent(JSON.stringify(result.pasueAt)) : ''});
  });
});



router.get('/video_b', function(req, res, next) {
	var group = req.query.group;
	console.log(group);
	var video = `video_b`;
	cmsData.getVideo(video).then(function(result){
		console.log(result);
    res.render('video_b', { video: result.video ? result.video.url : '', webvtt: result.webvtt ? result.webvtt.url : '', pauseAt: result.pasueAt ? encodeURIComponent(JSON.stringify(result.pasueAt)) : ''});
  });
});



router.get('/video_c', function(req, res, next) {
	var group = req.query.group;
	console.log(group);
	var video = `video_c`;
	cmsData.getVideo(video).then(function(result){
		console.log(result);
    res.render('video_c', { video: result.video ? result.video.url : '', webvtt: result.webvtt ? result.webvtt.url : '', pauseAt: result.pasueAt ? encodeURIComponent(JSON.stringify(result.pasueAt)) : ''});
  });
});


router.get('/video_d', function(req, res, next) {
	var group = req.query.group;
	console.log(group);
	var video = `video_d`;
	cmsData.getVideo(video).then(function(result){
		console.log(result);
    res.render('video_d', { video: result.video ? result.video.url : '', webvtt: result.webvtt ? result.webvtt.url : '', pauseAt: result.pasueAt ? encodeURIComponent(JSON.stringify(result.pasueAt)) : ''});
  });
});
//----------------------------------


router.get('/exp1-start', function(req, res, next) {
	cmsData.getInstruction('exp1-start').then(function(instruction){
	  res.render('exp1-start', { instruction: instruction });
	});
});

router.get('/exp1-intro', function(req, res, next) {
	cmsData.getInstruction('exp1-intro').then(function(instruction){
	  res.render('exp1-intro', { instruction: instruction });
	});
});

router.get('/exp1-profile', function(req, res, next) {
	cmsData.getSurvey('exp1-profile').then(function(survey){
	  res.render('exp1-profile', { survey: `/proxy-survey/surveys/${survey._id}` });
	});
});

router.get('/exp1-pre-test-intro', function(req, res, next) {
	cmsData.getInstruction('exp1-pre-test-intro').then(function(instruction){
	  res.render('exp1-pre-test-intro', { instruction: instruction });
	});
});

router.get('/exp1-pre-test', function(req, res, next) {
	cmsData.getSurvey('exp1-pre-test').then(function(survey){
	  res.render('exp1-pre-test', { survey: `/proxy-survey/surveys/${survey._id}` });
	});
});


router.get('/exp1-after-pre-video-test', function(req, res, next) {
	cmsData.getSurvey('exp1-after-pre-video-test').then(function(survey){
	  res.render('exp1-after-pre-video-test', { survey: `/proxy-survey/surveys/${survey._id}` });
	});
});


//------------//------------//------------//------------//------------//------------//------------//------------//------------
router.get('/exp1-feeling-test', function(req, res, next) {
	cmsData.getSurvey('exp1-feeling-test').then(function(survey){
	  res.render('exp1-feeling-test', { survey: `/proxy-survey/surveys/${survey._id}` });
	});
});


router.get('/exp1-video-questions', function(req, res, next) {
	cmsData.getSurvey('exp1-video-questions').then(function(survey){
	  res.render('exp1-video-questions', { survey: `/proxy-survey/surveys/${survey._id}` });
	});
});


router.get('/exp1-feeling-test-question', function(req, res, next) {
	cmsData.getSurvey('exp1-feeling-test-question').then(function(survey){
	  res.render('exp1-feeling-test-question', { survey: `/proxy-survey/surveys/${survey._id}` });
	});
});

router.get('/exp3-video2-intro-pre-b', function(req, res, next) {
	//cmsData.getInstruction('exp1-video-intro').then(function(instruction){
		res.render('exp3-video2-intro-pre-b');
	//});
});


router.get('/exp3-video2-intro-pre-b2', function(req, res, next) {
	//cmsData.getInstruction('exp1-video-intro').then(function(instruction){
		res.render('exp3-video2-intro-pre-b2');
	//});
});



router.get('/exp3-video2-intro-b', function(req, res, next) {
	//cmsData.getInstruction('exp1-video-intro').then(function(instruction){
		res.render('exp3-video2-intro-b');
	//});
});






router.get('/exp1-understanding-check', function(req, res, next) {
	cmsData.getSurvey('exp1-understanding-check').then(function(survey){
	  res.render('exp1-understanding-check', { survey: `/proxy-survey/surveys/${survey._id}` });
	});
});



//------------//------------//------------//------------//------------//------------//------------//------------//------------


//router.get('/exp1-video-intro', function(req, res, next) {
//	cmsData.getInstruction('exp1-video-intro').then(function(instruction){
//		res.render('exp1-video-intro', { instruction: instruction });
//	});
//});


router.get('/exp1-video-intro', function(req, res, next) {
	//cmsData.getInstruction('exp1-video-intro').then(function(instruction){
		res.render('exp1-video-intro');
	//});
});


router.get('/exp1-video-intro-a', function(req, res, next) {
	//cmsData.getInstruction('exp1-video-intro').then(function(instruction){
		res.render('exp1-video-intro-a');
	//});
});

router.get('/exp1-video-intro-b', function(req, res, next) {
	//cmsData.getInstruction('exp1-video-intro').then(function(instruction){
		res.render('exp1-video-intro-b');
	//});
});

router.get('/exp1-video-intro-c', function(req, res, next) {
	//cmsData.getInstruction('exp1-video-intro').then(function(instruction){
		res.render('exp1-video-intro-c');
	//});
});

router.get('/exp1-video-intro-d', function(req, res, next) {
	//cmsData.getInstruction('exp1-video-intro').then(function(instruction){
		res.render('exp1-video-intro-d');
	//});
});

router.get('/exp1-pre-video-intro-a', function(req, res, next) {
	// cmsData.getInstruction('exp1-video-intro').then(function(instruction){
		res.render('exp1-pre-video-intro-a');
	// });
});


router.get('/exp1-pre-video-intro-b', function(req, res, next) {
	// cmsData.getInstruction('exp1-video-intro').then(function(instruction){
		res.render('exp1-pre-video-intro-b');
	// });
});

router.get('/exp1-pre-video-intro-c', function(req, res, next) {
	// cmsData.getInstruction('exp1-video-intro').then(function(instruction){
		res.render('exp1-pre-video-intro-c');
	// });
});

router.get('/exp1-pre-video-intro-d', function(req, res, next) {
	// cmsData.getInstruction('exp1-video-intro').then(function(instruction){
		res.render('exp1-pre-video-intro-d');
	// });
});



router.get('/exp3-video2-intro-a', function(req, res, next) {
	//cmsData.getInstruction('exp1-video-intro').then(function(instruction){
		res.render('exp3-video2-intro-a');
	//});
});




router.get('/exp3-video2-intro-c', function(req, res, next) {
	//cmsData.getInstruction('exp1-video-intro').then(function(instruction){
		res.render('exp3-video2-intro-c');
	//});
});



router.get('/exp3-video3-intro-a', function(req, res, next) {
	//cmsData.getInstruction('exp1-video-intro').then(function(instruction){
		res.render('exp3-video3-intro-a');
	//});
});

router.get('/exp3-video3-intro-b', function(req, res, next) {
	//cmsData.getInstruction('exp1-video-intro').then(function(instruction){
		res.render('exp3-video3-intro-b');
	//});
});

router.get('/exp3-video3-intro-c', function(req, res, next) {
	//cmsData.getInstruction('exp1-video-intro').then(function(instruction){
		res.render('exp3-video3-intro-c');
	//});
});


router.get('/exp3-video4-intro-a', function(req, res, next) {
	//cmsData.getInstruction('exp1-video-intro').then(function(instruction){
		res.render('exp3-video4-intro-a');
	//});
});

router.get('/exp3-video4-intro-b', function(req, res, next) {
	//cmsData.getInstruction('exp1-video-intro').then(function(instruction){
		res.render('exp3-video4-intro-b');
	//});
});

router.get('/exp3-video4-intro-c', function(req, res, next) {
	//cmsData.getInstruction('exp1-video-intro').then(function(instruction){
		res.render('exp3-video4-intro-c');
	//});
});


router.get('/exp3-video1-a', function(req, res, next) {
	var group = req.query.group;
	console.log(group);
	var video = `exp3-video1-a`;
	cmsData.getVideo(video).then(function(result){
		console.log(result);
    res.render('exp3-video1-a', { video: result.video ? result.video.url : '', webvtt: result.webvtt ? result.webvtt.url : '', pauseAt: result.pasueAt ? encodeURIComponent(JSON.stringify(result.pasueAt)) : ''});
  });
});


router.get('/exp3-video1-b', function(req, res, next) {
	var group = req.query.group;
	console.log(group);
	var video = `exp3-video1-b`;
	cmsData.getVideo(video).then(function(result){
		console.log(result);
    res.render('exp3-video1-b', { video: result.video ? result.video.url : '', webvtt: result.webvtt ? result.webvtt.url : '', pauseAt: result.pasueAt ? encodeURIComponent(JSON.stringify(result.pasueAt)) : ''});
  });
});


router.get('/exp3-video1-c', function(req, res, next) {
	var group = req.query.group;
	console.log(group);
	var video = `exp3-video1-c`;
	cmsData.getVideo(video).then(function(result){
		console.log(result);
    res.render('exp3-video1-c', { video: result.video ? result.video.url : '', webvtt: result.webvtt ? result.webvtt.url : '', pauseAt: result.pasueAt ? encodeURIComponent(JSON.stringify(result.pasueAt)) : ''});
  });
});



router.get('/exp3-video2-a', function(req, res, next) {
	var group = req.query.group;
	console.log(group);
	var video = `exp3-video2-a`;
	cmsData.getVideo(video).then(function(result){
		console.log(result);
    res.render('exp3-video2-a', { video: result.video ? result.video.url : '', webvtt: result.webvtt ? result.webvtt.url : '', pauseAt: result.pasueAt ? encodeURIComponent(JSON.stringify(result.pasueAt)) : ''});
  });
});


router.get('/exp3-video2-b', function(req, res, next) {
	var group = req.query.group;
	console.log(group);
	var video = `exp3-video2-b`;
	cmsData.getVideo(video).then(function(result){
		console.log(result);
    res.render('exp3-video2-b', { video: result.video ? result.video.url : '', webvtt: result.webvtt ? result.webvtt.url : '', pauseAt: result.pasueAt ? encodeURIComponent(JSON.stringify(result.pasueAt)) : ''});
  });
});


router.get('/exp3-video2-c', function(req, res, next) {
	var group = req.query.group;
	console.log(group);
	var video = `exp3-video2-c`;
	cmsData.getVideo(video).then(function(result){
		console.log(result);
    res.render('exp3-video2-c', { video: result.video ? result.video.url : '', webvtt: result.webvtt ? result.webvtt.url : '', pauseAt: result.pasueAt ? encodeURIComponent(JSON.stringify(result.pasueAt)) : ''});
  });
});


router.get('/exp3-video3-a', function(req, res, next) {
	var group = req.query.group;
	console.log(group);
	var video = `exp3-video3-a`;
	cmsData.getVideo(video).then(function(result){
		console.log(result);
    res.render('exp3-video3-a', { video: result.video ? result.video.url : '', webvtt: result.webvtt ? result.webvtt.url : '', pauseAt: result.pasueAt ? encodeURIComponent(JSON.stringify(result.pasueAt)) : ''});
  });
});


router.get('/exp3-video3-b', function(req, res, next) {
	var group = req.query.group;
	console.log(group);
	var video = `exp3-video3-b`;
	cmsData.getVideo(video).then(function(result){
		console.log(result);
    res.render('exp3-video3-b', { video: result.video ? result.video.url : '', webvtt: result.webvtt ? result.webvtt.url : '', pauseAt: result.pasueAt ? encodeURIComponent(JSON.stringify(result.pasueAt)) : ''});
  });
});


router.get('/exp3-video3-c', function(req, res, next) {
	var group = req.query.group;
	console.log(group);
	var video = `exp3-video3-c`;
	cmsData.getVideo(video).then(function(result){
		console.log(result);
    res.render('exp3-video3-c', { video: result.video ? result.video.url : '', webvtt: result.webvtt ? result.webvtt.url : '', pauseAt: result.pasueAt ? encodeURIComponent(JSON.stringify(result.pasueAt)) : ''});
  });
});


router.get('/exp3-video4-a', function(req, res, next) {
	var group = req.query.group;
	console.log(group);
	var video = `exp3-video4-a`;
	cmsData.getVideo(video).then(function(result){
		console.log(result);
    res.render('exp3-video4-a', { video: result.video ? result.video.url : '', webvtt: result.webvtt ? result.webvtt.url : '', pauseAt: result.pasueAt ? encodeURIComponent(JSON.stringify(result.pasueAt)) : ''});
  });
});


router.get('/exp3-video4-b', function(req, res, next) {
	var group = req.query.group;
	console.log(group);
	var video = `exp3-video4-b`;
	cmsData.getVideo(video).then(function(result){
		console.log(result);
    res.render('exp3-video4-b', { video: result.video ? result.video.url : '', webvtt: result.webvtt ? result.webvtt.url : '', pauseAt: result.pasueAt ? encodeURIComponent(JSON.stringify(result.pasueAt)) : ''});
  });
});



router.get('/exp3-video4-c', function(req, res, next) {
	var group = req.query.group;
	console.log(group);
	var video = `exp3-video4-c`;
	cmsData.getVideo(video).then(function(result){
		console.log(result);
    res.render('exp3-video4-c', { video: result.video ? result.video.url : '', webvtt: result.webvtt ? result.webvtt.url : '', pauseAt: result.pasueAt ? encodeURIComponent(JSON.stringify(result.pasueAt)) : ''});
  });
});



router.get('/exp1-pre-video-a', function(req, res, next) {
	var group = req.query.group;
	console.log(group);
	var video = `exp1-pre-video-a`;
	cmsData.getVideo(video).then(function(result){
		console.log(result);
    res.render('exp1-pre-video-a', { video: result.video ? result.video.url : '', webvtt: result.webvtt ? result.webvtt.url : '', pauseAt: result.pasueAt ? encodeURIComponent(JSON.stringify(result.pasueAt)) : ''});
  });
});

router.get('/exp1-pre-video-b', function(req, res, next) {
	var group = req.query.group;
	console.log(group);
	var video = `exp1-pre-video-b`;
	cmsData.getVideo(video).then(function(result){
		console.log(result);
    res.render('exp1-pre-video-b', { video: result.video ? result.video.url : '', webvtt: result.webvtt ? result.webvtt.url : '', pauseAt: result.pasueAt ? encodeURIComponent(JSON.stringify(result.pasueAt)) : ''});
  });
});

router.get('/exp1-pre-video-c', function(req, res, next) {
	var group = req.query.group;
	console.log(group);
	var video = `exp1-pre-video-c`;
	cmsData.getVideo(video).then(function(result){
		console.log(result);
    res.render('exp1-pre-video-c', { video: result.video ? result.video.url : '', webvtt: result.webvtt ? result.webvtt.url : '', pauseAt: result.pasueAt ? encodeURIComponent(JSON.stringify(result.pasueAt)) : ''});
  });
});

router.get('/exp1-pre-video-d', function(req, res, next) {
	var group = req.query.group;
	console.log(group);
	var video = `exp1-pre-video-d`;
	cmsData.getVideo(video).then(function(result){
		console.log(result);
    res.render('exp1-pre-video-d', { video: result.video ? result.video.url : '', webvtt: result.webvtt ? result.webvtt.url : '', pauseAt: result.pasueAt ? encodeURIComponent(JSON.stringify(result.pasueAt)) : ''});
  });
});


router.get('/exp1-video', function(req, res, next) {
	var group = req.query.group;
	console.log(group);
	var video = `exp1-video-${group}`;
	cmsData.getVideo(video).then(function(result){
		console.log(result);
    res.render('exp1-video', { video: result.video ? result.video.url : '', webvtt: result.webvtt ? result.webvtt.url : '', pauseAt: result.pasueAt ? encodeURIComponent(JSON.stringify(result.pasueAt)) : ''});
  });
});

router.get('/exp1-video-a', function(req, res, next) {
	var group = req.query.group;
	console.log(group);
	var video = `exp1-video-${group}`;
	cmsData.getVideo(video).then(function(result){
		console.log(result);
    res.render('exp1-video-a', { video: result.video ? result.video.url : '', webvtt: result.webvtt ? result.webvtt.url : '', pauseAt: result.pasueAt ? encodeURIComponent(JSON.stringify(result.pasueAt)) : ''});
  });
});

router.get('/exp1-video-b', function(req, res, next) {
	var group = req.query.group;
	console.log(group);
	var video = `exp1-video-${group}`;
	cmsData.getVideo(video).then(function(result){
		console.log(result);
    res.render('exp1-video-b', { video: result.video ? result.video.url : '', webvtt: result.webvtt ? result.webvtt.url : '', pauseAt: result.pasueAt ? encodeURIComponent(JSON.stringify(result.pasueAt)) : ''});
  });
});

router.get('/exp1-video-c', function(req, res, next) {
	var group = req.query.group;
	console.log(group);
	var video = `exp1-video-${group}`;
	cmsData.getVideo(video).then(function(result){
		console.log(result);
    res.render('exp1-video-c', { video: result.video ? result.video.url : '', webvtt: result.webvtt ? result.webvtt.url : '', pauseAt: result.pasueAt ? encodeURIComponent(JSON.stringify(result.pasueAt)) : ''});
  });
});

router.get('/exp1-video-d', function(req, res, next) {
	var group = req.query.group;
	console.log(group);
	var video = `exp1-video-${group}`;
	cmsData.getVideo(video).then(function(result){
		console.log(result);
    res.render('exp1-video-d', { video: result.video ? result.video.url : '', webvtt: result.webvtt ? result.webvtt.url : '', pauseAt: result.pasueAt ? encodeURIComponent(JSON.stringify(result.pasueAt)) : ''});
  });
});

// router.get('/exp1-cognitive-load-test-intro', function(req, res, next) {
// 	cmsData.getInstruction('exp1-cognitive-load-test-intro').then(function(instruction){
// 	  res.render('exp1-cognitive-load-test-intro', { instruction: instruction });
// 	});
// });

router.get('/exp1-cognitive-load-test', function(req, res, next) {
	cmsData.getSurvey('exp1-cognitive-load-test').then(function(survey){
	  res.render('exp1-cognitive-load-test', { survey: `/proxy-survey/surveys/${survey._id}` });
	});
});

router.get('/exp1-post-test-intro', function(req, res, next) {
	cmsData.getInstruction('exp1-post-test-intro').then(function(instruction){
	  res.render('exp1-post-test-intro', { instruction: instruction });
	});
});

router.get('/exp1-post-test', function(req, res, next) {
	cmsData.getSurvey('exp1-post-test').then(function(survey){
	  res.render('exp1-post-test', { survey: `/proxy-survey/surveys/${survey._id}` });
	});
});

router.get('/exp1-control-related-questions-a', function(req, res, next) {
	cmsData.getSurvey('exp1-control-related-questions-a').then(function(survey){
	  res.render('exp1-control-related-questions-a', { survey: `/proxy-survey/surveys/${survey._id}` });
	});
});


router.get('/exp1-control-related-questions-b', function(req, res, next) {
	cmsData.getSurvey('exp1-control-related-questions-b').then(function(survey){
	  res.render('exp1-control-related-questions-b', { survey: `/proxy-survey/surveys/${survey._id}` });
	});
});


router.get('/exp1-control-related-questions-c', function(req, res, next) {
	cmsData.getSurvey('exp1-control-related-questions-c').then(function(survey){
	  res.render('exp1-control-related-questions-c', { survey: `/proxy-survey/surveys/${survey._id}` });
	});
});

router.get('/exp1-control-related-questions-d', function(req, res, next) {
	cmsData.getSurvey('exp1-control-related-questions-d').then(function(survey){
	  res.render('exp1-control-related-questions-d', { survey: `/proxy-survey/surveys/${survey._id}` });
	});
});


router.get('/exp1-post-load-test', function(req, res, next) {
	cmsData.getSurvey('exp1-post-load-test').then(function(survey){
	  res.render('exp1-post-load-test', { survey: `/proxy-survey/surveys/${survey._id}` });
	});
});



router.get('/exp1-end', function(req, res, next) {
	cmsData.getInstruction('exp1-end').then(function(instruction){
	  res.render('exp1-end', { instruction: instruction });
	});
});
// router.get('/exp1-use-guide-start', function(req, res, next) {
// 	cmsData.getInstruction('exp1-use-guide-start').then(function(instruction){
// 	  res.render('exp1-use-guide-start', { instruction: instruction });
// 	});
// });

// router.get('/exp1-use-video', function(req, res, next) {
// 	cmsData.getVideo('exp1-use-video').then(function(result){
//     res.render('exp1-use-video', { video: result.video ? result.video.url : '' });
//   });
// });

// router.get('/exp1-use-survey', function(req, res, next) {
// 	cmsData.getSurvey('exp1-use-survey').then(function(survey){
// 	  res.render('exp1-use-survey', { survey: `/proxy-survey/surveys/${survey._id}` });
// 	});
// });

// router.get('/exp1-use-guide-end', function(req, res, next) {
// 	cmsData.getInstruction('exp1-use-guide-end').then(function(instruction){
// 	  res.render('exp1-use-guide-end', { instruction: instruction });
// 	});
// });

module.exports = router;