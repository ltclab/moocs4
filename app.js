var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var api = require('./routes/api');

var app = express();

var proxy = require('express-http-proxy');
var url = require('url');

var config = require('config');

app.use('/proxy-cms-uploads', proxy(config.get('cms'), {
  proxyReqPathResolver: function(req) {
    return `/uploads/${url.parse(req.url).path}`;
  }
}));

app.use('/proxy-survey', proxy(config.get('survey'), {
  proxyReqPathResolver: function(req) {
    return url.parse(req.url).path;
  }
}));

var passport = require('passport');
var Strategy = require('passport-local').Strategy;
var ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn;
var cmsData = require('./lib/cms-data');

passport.use(new Strategy(
  function(username, password, cb) {
    cmsData.getUser(username, password).then(function(user){
      if ( user ) {
        return cb(null, user);
      } else {
        return cb(null, false);
      }
    });
  }));

passport.serializeUser(function(user, cb) {
  cb(null, user._id);
});

passport.deserializeUser(function(id, cb) {
  cmsData.getUserById(id).then(function(user){
    if ( user ) {
      return cb(null, user);
    } else {
      return cb(null, false);
    }
  });
});


// view engine setup!
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


app.use(require('express-session')({ secret: 'U9P7Er1VT0', resave: false, saveUninitialized: false }));
app.use(passport.initialize());
app.use(passport.session());

app.get('/login',
  function(req, res){
    var message = '';
    var message_code = req.query.message_code;
    if ( message_code === 'not_pass' ) {
      message = '帳號、密碼錯誤或是已過期';
    }
    res.render('login', { message: message });
  });

app.post('/login',
  passport.authenticate('local', { successReturnToOrRedirect: '/', failureRedirect: '/login?message_code=not_pass' }),
  function(req, res) {
    res.redirect('/');
  });

app.get('/logout',
  function(req, res){
    req.logout();
    res.redirect('/');
  });


var reqkey = config.get('reqkey');

app.use('/', ensureLoggedIn(), index);
app.use('/api', api);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;