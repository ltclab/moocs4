const rp = require('request-promise');
const config = require('config');
const resources = config.get('resources');
const apikey = config.get('apikey');

let cmsData = {};

cmsData.getInstruction = (type) => {
  let options = {
    method: 'GET',
    uri: `${resources.instructions}/${type}`,
    form: {
      apikey: apikey
    },
    json: true
  };

  return rp(options)
    .then(function (result) {
      return result.moocsInstruction ? result.moocsInstruction.post.content : null;
    })
    .catch(function (err) {
      console.error(err);
    });
};

cmsData.getSurvey = (type) => {
  let options = {
    method: 'GET',
    uri: `${resources.surveys}/${type}`,
    form: {
      apikey: apikey
    },
    json: true
  };

  return rp(options)
    .then(function (result) {
      return result.moocsSurvey ? result.moocsSurvey.survey : null;
    })
    .catch(function (err) {
      console.error(err);
    });
};

cmsData.getUser = (username, password) => {
  let options = {
    method: 'POST',
    uri: `${resources.users}/login`,
    form: {
      apikey: apikey,
      username: username,
      password: password
    },
    json: true
  };

  return rp(options)
    .then(function (result) {
      return result.moocsUser;
    })
    .catch(function (err) {
      console.error(err);
    });
};

cmsData.getUserById = (id) => {
  let options = {
    method: 'GET',
    uri: `${resources.users}/${id}`,
    form: {
      apikey: apikey
    },
    json: true
  };

  return rp(options)
    .then(function (result) {
      return result.moocsUser;
    })
    .catch(function (err) {
      console.error(err);
    });
};

cmsData.postSurveyResults = (req) => {
  try {
    let survey = req.body;

    var options = {
      method: 'POST',
      uri: `${resources.surveyResults}`,
      form: survey
    };

    return rp(options)
      .then(function (result) {
        return result;
      })
      .catch(function (err) {
        console.error(err);
      });
  } catch(error) {
    console.error(error);
  }
};

cmsData.getVideo = (type) => {
  let options = {
    method: 'GET',
    uri: `${resources.videos}/${type}`,
    form: {
      apikey: apikey
    },
    json: true
  };

  return rp(options)
    .then(function (result) {
      result.moocsVideo = result.moocsVideo || {}
      if ( result.moocsVideo.webvtt ) {
        let url = result.moocsVideo.webvtt.url;
        let sbustrs = url.split('/');
        let fileName = sbustrs[sbustrs.length -1];
        result.moocsVideo.webvtt.url = `/proxy-cms-uploads/${fileName}`;
      }
      return result.moocsVideo;
    })
    .catch(function (err) {
      console.error(err);
    });
};

cmsData.postEvents = (req) => {
  try {
    let event = req.body;

    var options = {
      method: 'POST',
      uri: `${resources.events}`,
      form: event
    };

    return rp(options)
      .then(function (result) {
        return result;
      })
      .catch(function (err) {
        console.error(err);
      });
  } catch(error) {
    console.error(error);
  }
};

module.exports = cmsData;